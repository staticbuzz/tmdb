import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

const Spinner = () => <p className="py-3 pl-3 text-center" style={{fontSize: '25px'}}>Fetching data... <FontAwesomeIcon color="#01d277" icon={faSpinner} spin={true} /></p>;

export default Spinner;