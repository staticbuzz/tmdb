import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

import './Landing.scss';
import Background from '../../images/bg.jpg';
import Logo from '../../images/logo.svg';
import TitleCard from './TitleCard/TitleCard';
import Spinner from '../Spinner/Spinner';

const Landing = () => {
    const [titles, setTitles] = useState([]);
    const [popularTitles, setPopularTitles] = useState([]);
    const [searchUrl, setSearchUrl] = useState(null);
    const [search, setSearch] = useState('');
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        const fetchPopularTitles = () => {
          fetch(`https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=6ed12e064b90ae1290fa326ce9e790ff`, { method: 'GET' })
          .then(result => result.json())
          .then(jsonData => {
              setPopularTitles(jsonData.results);
              setIsLoaded(true);
            });
        };
        fetchPopularTitles();
    }, []);

    useEffect(() => {
        const fetchTitles = () => {
          if (search) {
            fetch(searchUrl, { method: 'GET' })
            .then(result => result.json())
            .then(jsonData => {
                setTitles(jsonData.results);
                });
            } else {
                setTitles([]);
            }
        };
        fetchTitles();
    }, [searchUrl, search]);

    const handleSearch = query => {
        setSearch(query);
        setSearchUrl(`https://api.themoviedb.org/3/search/movie?api_key=6ed12e064b90ae1290fa326ce9e790ff&query=${query}`);
    }

    return (
        <div className="Landing">
            <Header />
            <div className="container" style={{marginTop: '-30px'}}>
                <div className="row">
                    <div className="col-12 pt-3">
                        <div className="input-group mb-3">
                            <input value={search} onChange={event => handleSearch(event.target.value)} type="text" className="form-control" placeholder="Search" aria-label="Search" aria-describedby="search" />
                            <div className="input-group-append">
                                <span className="input-group-text" id="search"><FontAwesomeIcon color="#01d277" icon={faSearch} /></span>
                            </div>
                        </div>
                    </div>
                </div>
                { search && <SearchedTitles titles={titles} /> }
                { isLoaded ? <PopularTitles popularTitles={popularTitles} /> : <Spinner /> }
            </div >
        </div>
    )                 
}

const Header = () => (
    <div className="container-fluid px-0">
        <div className="logo-cont">
            <img src={Logo} alt="logo" className="logo-img"/>
        </div>
        <img src={Background} alt="background stripes" className="bg-img"/>
    </div>
);

const SearchedTitles = props => (
    <div className="row mb-3">
        <div className="col-12">
            <h2 className="pt-4 pb-2">Search Results</h2>
        </div>
        {props.titles.map((e, i) => (
            <div key={i} className="col-6 col-lg-3">
                <TitleCard data={e}></TitleCard>
            </div>
        ))}
        { !props.titles.length && <div className="col-12"><p>Sorry... your search returned no results</p></div> }
    </div>
);

const PopularTitles = props => (
    <div className="row mb-3">
        <div className="col-12">
            <h2 className="pt-4 pb-2">Popular Movies</h2>
        </div>
        {props.popularTitles.map((e, i) => (
            <div key={i} className="col-6 col-lg-3">
                <TitleCard data={e}></TitleCard>
            </div>
        ))}
    </div>
);

export default Landing;
