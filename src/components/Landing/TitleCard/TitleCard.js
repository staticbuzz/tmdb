import React from 'react';
import { Link } from 'react-router-dom';
import * as moment from  'moment';

import MissingImage from '../../../images/no-pic.png';
import './TitleCard.scss';

const TitleCard = props => {
    const {
        title,
        poster_path: posterPath,
        id, 
        release_date: releaseDate,
        vote_average: voteAverage
    } = props.data;

    const badgeClass = 'badge rating-badge ' + (voteAverage <= '3.3' ? 'badge-danger' : (voteAverage <= '6.6' ? 'badge-primary' : 'badge-success'));
    const basePath = 'https://image.tmdb.org/t/p/w300';

    return (
        <Link to={`./${id}`}>
            <span className={badgeClass}>{getVoteAverage(voteAverage)}</span>
            <img className="img-fluid card-img" alt={`${title} poster`} src={posterPath ? basePath + posterPath : MissingImage}></img>
            <p className="title-text">{title}</p>
            <p className="release-date-text">{getReleaseMonthYear(releaseDate)}</p>
        </Link>
    )
}

const getReleaseMonthYear = date => date ? <span>{moment(date, 'YYYY-MM-DD').format('MMMM YYYY')}</span> : <span>Release Date Unknown</span>;

const getVoteAverage = voteAverage => <span>{voteAverage * 10}%</span>;

export default TitleCard;