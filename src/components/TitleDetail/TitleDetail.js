import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import * as moment from 'moment';

import './TitleDetail.scss';
import MissingImage from '../../images/no-pic.png';
import Spinner from '../Spinner/Spinner';

const TitleDetail = props => {
    const [data, setData] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);
    const { match: { params } } = props;
    const url = `https://api.themoviedb.org/3/movie/${params.id}?api_key=6ed12e064b90ae1290fa326ce9e790ff`;
    

    useEffect(() => {
        const fetchData = () => {
          fetch(url, { method: 'GET' })
          .then(result => result.json())
          .then(jsonData => {
              setData(jsonData);
              setIsLoaded(true);
            });
        };
        fetchData();
    }, [url]);

    return (
        isLoaded ? 
        <div className="TitleDetail">
            <Backdrop backdropPath={data.backdrop_path} title={data.title} />
            <div className="container">
                <div className="row">
                    <Poster posterPath={data.poster_path} title={data.title}/>
                    <AdditionalInfo releaseDate={data.release_date} title={data.title} voteAvg={data.vote_average} runtime={data.runtime} />
                </div>
                <Overview overview={data.overview} />
            </div>
        </div > : 
        <Spinner />
    )
}

const imgBasePath = 'https://image.tmdb.org/t/p';

const Backdrop = props => (
    <div className="container-fluid px-0 mx-0 no-backdrop">
        <Link className="backBtn" to={'../'}><FontAwesomeIcon color="white" icon={faArrowLeft} /></Link>
        {props.backdropPath && <img alt={`${props.title} backdrop`} src={imgBasePath + '/original' + props.backdropPath} />}
    </div>
);

const Poster = props => (
    <div className="col-5 pr-0" style={{minHeight: '250px'}}>
        <img className="img-fluid poster" alt={`${props.title} poster`} src={props.posterPath ? imgBasePath + '/w200' + props.posterPath : MissingImage} />
    </div>
);

const AdditionalInfo = props => (
    <div className="col-7 pl-4">
        <h1 className="pt-3 pb-2">{props.title}</h1>
        <p className="mb-0">{getReleaseYear(props.releaseDate)}{getUserScore(props.voteAvg)}</p>
        <p>{getRunTime(props.runtime)}</p>
    </div>
); 

const Overview = props => (
    <div className="row overview">
        <div className="col-12">
            <hr></hr>
            <h2 className="py-2">Overview</h2>
            <p>{props.overview}</p>
        </div>
    </div>
);

const getUserScore = voteAvg => {
    return <span>{voteAvg * 10}% User Score</span>
}

const getReleaseYear = releaseDate => {
    return releaseDate ? <span>{moment(releaseDate, 'YYYY-MM-DD').format('YYYY')} • </span> : null
}

const getRunTime = runtime => {
    const hours = Math.floor(runtime / 60);
    const mins = runtime % 60;
    return <span>{hours}h {mins} min</span>
}

export default TitleDetail;