import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Landing from './components/Landing/Landing';
import TitleDetail from './components/TitleDetail/TitleDetail';
import './App.scss';

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route path="/:id" component={TitleDetail} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
